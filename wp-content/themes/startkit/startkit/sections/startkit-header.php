<?php
	$hide_show_social_icon		= get_theme_mod('hide_show_social_icon','1');
	$social_icons				= get_theme_mod('social_icons');
	$startkit_address_icon		= get_theme_mod('startkit_address_icon');
	$startkit_address			= get_theme_mod('startkit_address');
	$hide_show_contact_infot	= get_theme_mod('hide_show_contact_infot','1');
	$header_email_icon			= get_theme_mod('header_email_icon');
	$header_email				= get_theme_mod('header_email');
	$header_phone_icon			= get_theme_mod('header_phone_icon'); 
	$header_phone_number		= get_theme_mod('header_phone_number'); 
	$sticky_header_setting		= get_theme_mod('sticky_header_setting','1'); 
?>
    <!-- Start: Header Top
    ============================= -->
	<?php if ( get_header_image() ) : ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="custom-header" rel="home">
			<img src="<?php esc_url(header_image()); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>">
		</a>
	<?php endif;  ?>
    <section id="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-md-left text-center mb-lg-0 mb-1 startkit-top-left">
				<?php if($hide_show_social_icon == '1') { ?>
                    <ul class="header-social d-inline-block">
						<?php
							$social_icons = json_decode($social_icons);
							if( $social_icons!='' )
							{
							foreach($social_icons as $social_item){	
								$social_icon = ! empty( $social_item->icon_value ) ? apply_filters( 'startkit_translate_single_string', $social_item->icon_value, 'Header section' ) : '';	
								$social_link = ! empty( $social_item->link ) ? apply_filters( 'startkit_translate_single_string', $social_item->link, 'Header section' ) : '';	
								?>	
									<li><a href="<?php echo esc_url( $social_link ); ?>" ><i class="fa <?php echo esc_attr( $social_icon ); ?> "></i></a></li>
						<?php } } ?>
                    </ul>
					<?php }	?>
                    <div class="address d-inline-block"><i class="fa <?php echo esc_attr( $startkit_address_icon ); ?> mr-2"></i><?php echo esc_html($startkit_address); ?></div>
                </div>
			<?php if($hide_show_contact_infot == '1' ) { ?>
                <div class="col-lg-6 col-md-6 text-center text-md-right startkit-top-right">
                    <div class="email d-inline-block">
                        <a href="<?php echo esc_url( 'mailto:' . antispambot( $header_email ) ) ?>"><i class="fa <?php echo esc_attr( $header_email_icon ); ?> mr-2"></i><?php echo antispambot( $header_email ); ?></a>
                    </div>
                    <div class="phone d-inline-block">
                        <i class="fa <?php echo esc_attr( $header_phone_icon ); ?> mr-2"></i><a href="<?php echo esc_url( 'tel:' . $header_phone_number ); ?>"><?php echo esc_html( $header_phone_number ); ?></a>
                    </div>
                </div>
			<?php } ?>
            </div>
        </div>
    </section>
    <!-- End: Header Top
    ============================= -->

    <!-- Start: Search
    ============================= -->

    <div id="search">
        <a href="#" id="close-btn">
        <i class="fa fa-times"></i>
        </a>
        <div>
		<form method="get" id="searchform" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <input id="searchbox" class="search-field" type="search" type="text" value="" name="s" id="s" placeholder="<?php esc_attr_e('type here','startkit'); ?>" />
            <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
		</form>
        </div>
    </div>
	
    <!-- End: Search
    ============================= -->
	