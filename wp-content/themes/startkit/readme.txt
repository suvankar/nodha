=== StartKit ===

Contributors: Nayrathemes
Requires at least: WordPress 4.4
Tested up to: WordPress 5.2.2
Requires PHP: 5.6
Stable tag: 1.1.10
Version: 1.1.10
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, left-sidebar, right-sidebar, flexible-header, custom-background, custom-colors, custom-header, custom-menu,  custom-logo, featured-image-header, featured-images, footer-widgets, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready, blog, e-commerce, portfolio, editor-style, grid-layout

== Copyright ==

StartKit WordPress Theme, Copyright 2019 Nayra Themes
StartKit is distributed under the terms of the GNU GPL


== Description ==

StartKit is a highly customizable theme created for all kind of business, info, and service websites. Install, customize, create easily with the included powerfull tools without touching a code. We have analyzed hundreds of business, consulting, finance, landing pages, product pages, corporate business, digital agency, product showcase, financial advisor, services, ecommerce and similar type of websites. StartKit has come with popular plugins support WPML, Polylang, WooCommerce, Contact Form 7, Revolution Slider, Elementor, Visual Composer, WP-Forms, Ninja Forms, Jetpack, Give (WordPress Donation Plugin), Gravity Forms, Yoast SEO and many more. View the demo of StartKit Lite http://www.nayrathemes.com/demo/lite/startkit/

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

StartKit includes support for Infinite Scroll in Jetpack and other following Plugins.
1. Clever Fox

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in Startkit WordPress Theme
-------------------------------------------------------------------------------------------------

1) Package Structure
Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

2) Font Awesome
=================================================================================================
Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
License - Font: SIL OFL 1.1, CSS: MIT License(http://fontawesome.io/license)
Source: http://fontawesome.io

3) Bootstrap Framework
=================================================================================================
Bootstrap (http://getbootstrap.com/)
Copyright (c) 2011-2019 Twitter, Inc.
License - MIT License (https://github.com/twbs/bootstrap/blob/master/LICENSE)

4) Owl Carousel 2.2.1
=================================================================================================
Owl Carousel 2.2.1 - by @David Deutsch - https://github.com/OwlCarousel2
License - MIT License(https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE)

5) Animate Css
=================================================================================================
Animate Css by @Daniel Eden - https://daneden.me/
License - MIT License (https://github.com/daneden/animate.css/blob/master/LICENSE)
Source: https://github.com/daneden/animate.css

6) Sticky Js
=================================================================================================
Sticky Js by @Anthony Garand - http://stickyjs.com/
License - MIT License (https://github.com/garand/sticky/blob/master/LICENSE.md)
Source: http://stickyjs.com/

7) Meanmenu Js
=================================================================================================
jquery.meanmenu.js by Chris Wharton
License - GNU LESSER GENERAL PUBLIC LICENSE (https://github.com/meanthemes/meanMenu/blob/master/gpl.txt)
Source: http://www.meanthemes.com/plugins/meanmenu/

8) Sainitization
=================================================================================================
License - GNU General Public License (https://github.com/WPTRT/code-examples/blob/master/LICENSE)
Source: https://github.com/WPTRT/code-examples

9) wp-bootstrap-navwalker
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt)
Source: https://github.com/wp-bootstrap/wp-bootstrap-navwalker

10) pure-css-toggle-buttons
=================================================================================================
License -   MIT license (https://github.com/soderlind/class-customizer-toggle-control/blob/master/pure-css-toggle-buttons/license.txt)
Source: https://github.com/soderlind/class-customizer-toggle-control

11) WordPress Customizer Toggle Control
=================================================================================================
License -  GNU GENERAL PUBLIC LICENSE (https://www.gnu.org/licenses/)
Source: https://github.com/soderlind/class-customizer-toggle-control

12) Screenshot Image
=================================================================================================
Screenshot Image
URL: https://stocksnap.io/photo/DWLWL9USBG
Source: https://stocksnap.io
License: CC0 Public Domain (https://stocksnap.io/license)

13) Image Folder Images
=================================================================================================
All other Images have been used in images folder, Created by Nayra Themes. Also they are GPL Licensed and free to use and free to redistribute further.


== Changelog ==
@version 1.1.10
* Header Style Change Setting Added

@version 1.1.9
* Fixed Style Issues

@version 1.1.8
* Tested With WordPress 5.2.2

@version 1.1.7
* Code Improvement

@version 1.1.6
* Demo Link Added In Theme Description

@version 1.1.5
* Theme Description Changed

@version 1.1.4
* Fixed Style issue
* Fixed Theme Check Bug

@version 1.1.3
* Remove Slider Background Pattern
* Screenshot Updated

@version 1.1.2
* Added Documentation & Support Links

@version 1.1.1
* Added Gutenberg Support

@version 1.1
* Fixed Style Issue
* Payment icon Added 

@version 1.0.14
* Fixed Style Issue
* Footer payment icon issue resolved

@version 1.0.13
* Remove Unnecessary Code

@version 1.0.12
* Added Limitation in Service section.
* Added Limitation in Testimonial section.
* Remove Custom Color Feature

@version 1.0.11
* Theme review bugs resolved

@version 1.0.10
* Theme review bugs resolved

@version 1.0.8
* Initial release