<?php 
// Customizer tabs
if ( ! function_exists( 'startkit_slider_manager_customize_register' ) ) :
function startkit_slider_manager_customize_register( $wp_customize ) {
	if ( class_exists( 'startkit_Customize_Control_Tabs' ) ) {

		// Pricing Tables Tabs
		$wp_customize->add_setting(
			'startkit_slider_tabs', array(
				'sanitize_callback' => 'sanitize_text_field',
			)
		);

		$wp_customize->add_control(
			new startkit_Customize_Control_Tabs(
				$wp_customize, 'startkit_slider_tabs', array(
					'section' => 'slider_setting',
					'tabs' => array(
						'general' => array(
							'nicename' => esc_html__( 'setting', 'startkit' ),
							'icon' => 'cogs',
							'controls' => array(
								'hide_show_slider',
								
							),
						),
						'Content' => array(
							'nicename' => esc_html__( 'Default', 'startkit' ),
							'icon' => 'table',
							'controls' => array(
								'slide_image',
								'slide_title',
								'slide_description',
								'slide_btn_text',
								'slide_btn_link',
								'slider_opacity',
							),
						),
					),
					
				)
			)
		);
	}
}
add_action( 'customize_register', 'startkit_slider_manager_customize_register' );
endif;

// Customizer tabs
function startkit_info_customize_register( $wp_customize ) {
	if ( class_exists( 'startkit_Customize_Control_Tabs' ) ) {

		// Pricing Tables Tabs
		$wp_customize->add_setting(
			'startkit_info_tabs', array(
				'sanitize_callback' => 'sanitize_text_field',
			)
		);

		$wp_customize->add_control(
			new startkit_Customize_Control_Tabs(
				$wp_customize, 'startkit_info_tabs', array(
					'section' => 'info_setting',
					'tabs' => array(
						'general' => array(
							'nicename' => esc_html__( 'Setting', 'startkit' ),
							'icon' => 'cogs',
							'controls' => array(
								'hide_show_info',
								
							),
						),
						'first' => array(
							'nicename' => esc_html__( 'First', 'startkit' ),
							'icon' => 'info',
							'controls' => array(
								'info_icons',
								'info_title',
								'info_description',
							),
						),
						'second' => array(
							'nicename' => esc_html__( 'Second', 'startkit' ),
							'icon' => 'info',
							'controls' => array(
								'info_icons2',
								'info_title2',
								'info_description2',
							),
						),
						'third' => array(
							'nicename' => esc_html__( 'Third', 'startkit' ),
							'icon' => 'info',
							'controls' => array(
								'info_icons3',
								'info_title3',
								'info_description3',	
							),
						),
					),
				)
			)
		);
	}
}

add_action( 'customize_register', 'startkit_info_customize_register' );
?>