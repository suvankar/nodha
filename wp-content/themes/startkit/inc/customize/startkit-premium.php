<?php
function startkit_premium_setting( $wp_customize ) {
	
/*=========================================
	Premium Theme Details Page
	=========================================*/

	/*=========================================
	Page Layout Settings Section
	=========================================*/
	$wp_customize->add_section(
        'upgrade_premium',
        array(
            'title' 		=> __('Need Help ?','startkit'),
		)
    );
	
	/*=========================================
	Add Buttons
	=========================================*/
	
	class WP_Button_Customize_Control extends WP_Customize_Control {
	public $type = 'upgrade_premium';

	   function render_content() {
		?>
			<div class="premium_info">
				<ul>
					<li><a href="http://help.nayrathemes.com/category/free-themes/startkit-free/" target="_blank"><i class="dashicons dashicons-visibility"></i><?php _e( 'Documentation','startkit' ); ?> </a></li>
					
					<li><a href="https://nayrathemes.ticksy.com/" target="_blank"><i class="dashicons dashicons-sos"></i><?php _e( 'Need Help ?','startkit' ); ?> </a></li>
				</ul>
			</div>
		<?php
	   }
	}
	
	$wp_customize->add_setting(
		'premium_info_buttons',
		array(
		   'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'startkit_sanitize_text',
		)	
	);
	
	$wp_customize->add_control( new WP_Button_Customize_Control( $wp_customize, 'premium_info_buttons', array(
		'section' => 'upgrade_premium',
		'setting' => 'premium_info_buttons',
    ))
);
}
add_action( 'customize_register', 'startkit_premium_setting' );
?>