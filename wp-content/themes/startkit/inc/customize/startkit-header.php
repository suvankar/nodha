<?php
function startkit_header_setting( $wp_customize ) {
$selective_refresh = isset( $wp_customize->selective_refresh ) ? 'postMessage' : 'refresh';
	/*=========================================
	Header Settings Panel
	=========================================*/
	$wp_customize->add_panel( 
		'header_section', 
		array(
			'priority'      => 127,
			'capability'    => 'edit_theme_options',
			'title'			=> __('Header Section', 'startkit'),
		) 
	);
	

	/*=========================================
	Header Settings Section
	=========================================*/
	// Header Settings Section // 
	$wp_customize->add_section(
        'header_style_setting',
        array(
        	'priority'      => 1,
            'title' 		=> __('Header Style','startkit'),
			'panel'  		=> 'header_section',
		)
    );
	// Header Style Setting // 
	$wp_customize->add_setting( 
		'enable_simple_header' , 
			array(
			'default' => 0,
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'enable_simple_header', 
		array(
			'label'	      => esc_html__( 'Enable Simple Header', 'startkit' ),
			'section'     => 'header_style_setting',
			'settings'    => 'enable_simple_header',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header Settings Section // 
	$wp_customize->add_section(
        'header_setting',
        array(
        	'priority'      => 1,
            'title' 		=> esc_html__('Top Left Header','startkit'),
			'panel'  		=> 'header_section',
		)
    );
	// Social Icons Hide/Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_social_icon' , 
			array(
			'default' => esc_html__( '1', 'startkit' ),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'hide_show_social_icon', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'startkit' ),
			'section'     => 'header_setting',
			'settings'    => 'hide_show_social_icon',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	/**
	 * Customizer Repeater
	 */
		$wp_customize->add_setting( 'social_icons', 
			array(
			 'sanitize_callback' => 'startkit_repeater_sanitize',
		)
		);
		
		$wp_customize->add_control( 
			new Startkit_Repeater( $wp_customize, 
				'social_icons', 
					array(
						'label'   => esc_html__('Social Icons','startkit'),
						'section' => 'header_setting',
						'priority' => 2,
						'customizer_repeater_icon_control' => true,
						'customizer_repeater_link_control' => true,
					) 
				) 
			);
	

	// address info icon startkit // 
	$wp_customize->add_setting(
    	'startkit_address_icon',
    	array(
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Startkit_Customizer_Icon_Picker_Control($wp_customize, 
		'startkit_address_icon',
		array(
		    'label'   		=> esc_html__('Address Icon','startkit'),
			'iconset' => 'fa',
		    'section' 		=> 'header_setting',
			'settings' 		 => 'startkit_address_icon',
		))  
	);
	
	// address info startkit // 
	$wp_customize->add_setting(
    	'startkit_address',
    	array(
			'sanitize_callback' => 'startkit_sanitize_html',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
		)
	);	

	$wp_customize->add_control( 
		'startkit_address',
		array(
		    'label'   		=> esc_html__('Address','startkit'),
		    'section' 		=> 'header_setting',
			'settings' 		 => 'startkit_address',
			'type'		 =>	'text'
		)  
	);
	/*=========================================
	Header Contact Settings Section
	=========================================*/
	// Header Contact Setting Section // 
	$wp_customize->add_section(
        'header_contact',
        array(
        	'priority'      => 2,
            'title' 		=> esc_html__('Top Right Header','startkit'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );
	
	// Header Contact Indo Hide/Show Setting // 
	
	// Social Icons Hide/Show Setting // 
	$wp_customize->add_setting( 
		'hide_show_contact_infot' , 
			array(
			'default' => esc_html__('1','startkit'),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'hide_show_contact_infot', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'startkit' ),
			'section'     => 'header_contact',
			'settings'    => 'hide_show_contact_infot',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header Email icon Setting // 
	$wp_customize->add_setting(
    	'header_email_icon',
    	array(
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Startkit_Customizer_Icon_Picker_Control($wp_customize, 
		'header_email_icon',
		array(
		    'label'   => esc_html__('Email Icon','startkit'),
		    'section' => 'header_contact',
			'settings'=> 'header_email_icon',
			'iconset' => 'fa',
		))  
	);
	
	// Header Email Setting // 
	$wp_customize->add_setting(
    	'header_email',
    	array(
			'sanitize_callback' => 'startkit_sanitize_html',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
		)
	);	

	$wp_customize->add_control( 
		'header_email',
		array(
		    'label'   => esc_html__('Email','startkit'),
		    'section' => 'header_contact',
			'settings'=> 'header_email',
			'type' => 'text',
		)  
	);

	// Header Contact Number Setting // 

	$wp_customize->add_setting(
    	'header_phone_icon',
    	array(
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Startkit_Customizer_Icon_Picker_Control($wp_customize, 
		'header_phone_icon',
		array(
		    'label'   => esc_html__('Phone Icon','startkit'),
		    'section' => 'header_contact',
			'settings'=> 'header_phone_icon',
			'iconset' => 'fa',
		))  
	);
	
	$wp_customize->add_setting(
    	'header_phone_number',
    	array(
			'sanitize_callback' => 'startkit_sanitize_html',
			'capability' => 'edit_theme_options',
			'transport'         => $selective_refresh,
		)
	);	

	$wp_customize->add_control( 
		'header_phone_number',
		array(
		    'label'   => esc_html__('Phone Number','startkit'),
		    'section' => 'header_contact',
			'settings'=> 'header_phone_number',
			'type' => 'text',
		)  
	);
	
	/*=========================================
	Header search & cart Settings Section
	=========================================*/
	// Header search & cart Setting Section // 
	$wp_customize->add_section(
        'header_contact_cart',
        array(
        	'priority'      => 3,
            'title' 		=> esc_html__('Header Search','startkit'),
			'panel'  		=> 'header_section',
		)
    );
	// hide/show  search & cart settings
	$wp_customize->add_setting( 
		'cart_header_setting' , 
			array(
			'default' => esc_html__('1','startkit'),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'cart_header_setting', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'startkit' ),
			'section'     => 'header_contact_cart',
			'settings'    => 'cart_header_setting',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	
	// Header search Setting // 
	$wp_customize->add_setting(
    	'header_search',
    	array(
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Startkit_Customizer_Icon_Picker_Control($wp_customize,  
		'header_search',
		array(
		    'label'   => esc_html__('Search Icon','startkit'),
		    'section' => 'header_contact_cart',
			'settings'=> 'header_search',
			'iconset' => 'fa',
		))  
	);
		
	
	/*=========================================
	 Header booknow button Section
	=========================================*/
	$wp_customize->add_section(
        'header_booknow',
        array(
        	'priority'      => 4,
            'title' 		=> esc_html__('Button Setting','startkit'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );
	
	// hide/show  search & cart settings
	$wp_customize->add_setting( 
		'booknow_setting' , 
			array(
			'default' => esc_html__('1','startkit'),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			'transport'         => $selective_refresh,
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'booknow_setting', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'startkit' ),
			'section'     => 'header_booknow',
			'settings'    => 'booknow_setting',
			'type'        => 'ios', // light, ios, flat
		) 
	));
	// Header button icon Setting // 
	$wp_customize->add_setting(
    	'header_btn_icon',
    	array(
	        'default'			=> esc_html__('fa-book','startkit'),
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control(new Startkit_Customizer_Icon_Picker_Control($wp_customize,  
		'header_btn_icon',
		array(
		    'label'   => esc_html__('Button Icon','startkit'),
		    'section' => 'header_booknow',
			'settings'=> 'header_btn_icon',
			'iconset' => 'fa',
		))  
	);
	// Header button label Setting // 
	$wp_customize->add_setting(
    	'header_btn_lbl',
    	array(
			'sanitize_callback' => 'startkit_sanitize_text',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control( 
		'header_btn_lbl',
		array(
		    'label'   => esc_html__('Button label','startkit'),
		    'section' => 'header_booknow',
			'settings'=> 'header_btn_lbl',
			'type' => 'text',
		)  
	);
	
	// Header button link Setting // 
	$wp_customize->add_setting(
    	'header_btn_link',
    	array(
			'sanitize_callback' => 'startkit_sanitize_url',
			'capability' => 'edit_theme_options',
		)
	);	

	$wp_customize->add_control( 
		'header_btn_link',
		array(
		    'label'   => esc_html__('Button link','startkit'),
		    'section' => 'header_booknow',
			'settings'=> 'header_btn_link',
			'type' => 'text',
		)  
	);
	/*=========================================
	Sticky Header Section
	=========================================*/
	$wp_customize->add_section(
        'sticky_header',
        array(
        	'priority'      => 3,
            'title' 		=> esc_html__('Sticky Header','startkit'),
            'description' 	=>'',
			'panel'  		=> 'header_section',
		)
    );
	
	
	$wp_customize->add_setting( 
		'sticky_header_setting' , 
			array(
			'default' =>  esc_html__('1','startkit'),
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
		) 
	);
	
	$wp_customize->add_control( new Startkit_Customizer_Toggle_Control( $wp_customize, 
	'sticky_header_setting', 
		array(
			'label'	      => esc_html__( 'Hide / Show Section', 'startkit' ),
			'section'     => 'sticky_header',
			'settings'    => 'sticky_header_setting',
			'type'        => 'ios', // light, ios, flat
		) 
	));
}

add_action( 'customize_register', 'startkit_header_setting' );
?>

<?php

// header selective refresh
function startkit_home_header_section_partials( $wp_customize ){

// hide show contact
	$wp_customize->selective_refresh->add_partial(
		'booknow_setting', array(
			'selector' => '.book-now-btn',
			'container_inclusive' => true,
			'render_callback' => 'header_booknow',
			'fallback_refresh' => true,
		)
	);
	// hide_show_social_icon
	$wp_customize->selective_refresh->add_partial(
		'hide_show_social_icon', array(
			'selector' => '.startkit-top-left',
			'container_inclusive' => true,
			'render_callback' => 'header_section',
			'fallback_refresh' => true,
		)
	);
	// hide show contact
	$wp_customize->selective_refresh->add_partial(
		'hide_show_contact_infot', array(
			'selector' => '.startkit-top-right',
			'container_inclusive' => true,
			'render_callback' => 'header_contact',
			'fallback_refresh' => true,
		)
	);
	// hide show cart 
	$wp_customize->selective_refresh->add_partial(
		'cart_header_setting', array(
			'selector' => '.search-cart-se',
			'container_inclusive' => true,
			'render_callback' => 'header_contact_cart',
			'fallback_refresh' => true,
		)
	);
	// social icons
	$wp_customize->selective_refresh->add_partial( 'social_icons', array(
		'selector'            => '#header-top .header-social',
		'settings'            => 'social_icons',
		'render_callback'  => 'startkit_header_section_social_render_callback',
	
	) );
	// address 
	$wp_customize->selective_refresh->add_partial( 'startkit_address', array(
		'selector'            => '#header-top .address',
		'settings'            => 'startkit_address',
		'render_callback'  => 'startkit_header_section_add_icon_render_callback',
	
	) );
	
	// email
		
	$wp_customize->selective_refresh->add_partial( 'header_email_icon', array(
		'selector'            => '#header-top .email i',
		'settings'            => 'header_email_icon',
		'render_callback'  => 'startkit_header_section_email_render_callback',
	
	) );
	
	// email text
		
	$wp_customize->selective_refresh->add_partial( 'header_email', array(
		'selector'            => '#header-top .email ',
		'settings'            => 'header_email',
		'render_callback'  => 'startkit_header_section_header_email_render_callback',
	
	) );
	
	// header_phone_icon
		
	$wp_customize->selective_refresh->add_partial( 'header_phone_icon', array(
		'selector'            => '#header-top .phone i',
		'settings'            => 'header_phone_icon',
		'render_callback'  => 'startkit_header_section_phone_render_callback',
	
	) );
	
	// header_phone_number
		
	$wp_customize->selective_refresh->add_partial( 'header_phone_number', array(
		'selector'            => '#header-top .phone ',
		'settings'            => 'header_phone_number',
		'render_callback'  => 'startkit_header_section_header_phone_number_render_callback',
	
	) );
	// header_search_icon
		
	$wp_customize->selective_refresh->add_partial( 'header_search', array(
		'selector'            => '#header .header-right-bar .search-button i',
		'settings'            => 'header_search',
		'render_callback'  => 'startkit_header_section_search_render_callback',
	
	) );
	
	// header_cart_icon
		
	$wp_customize->selective_refresh->add_partial( 'header_cart', array(
		'selector'            => '#header .header-right-bar .cart-icon i',
		'settings'            => 'header_cart',
		'render_callback'  => 'startkit_header_section_cart_render_callback',
	
	) );
	// book now button
		
	$wp_customize->selective_refresh->add_partial( 'header_btn_lbl', array(
		'selector'            => '#header .header-right-bar .book-now-btn',
		'settings'            => 'header_btn_lbl',
		'render_callback'  => 'startkit_header_section_booknow_render_callback',
	
	) );
	}

add_action( 'customize_register', 'startkit_home_header_section_partials' );

// social icons
function startkit_header_section_social_render_callback() {
	return get_theme_mod( 'social_icons' );
}
// address icon
function startkit_header_section_add_icon_render_callback() {
	return get_theme_mod( 'startkit_address' );
}
// email
function startkit_header_section_email_render_callback() {
	return get_theme_mod( 'header_email_icon' );
}

// header_email
function startkit_header_section_header_email_render_callback() {
	return get_theme_mod( 'header_email' );
}
// phone 
function startkit_header_section_phone_render_callback() {
	return get_theme_mod( 'header_phone_icon' );
}
// header_phone_number 
function startkit_header_section_header_phone_number_render_callback() {
	return get_theme_mod( 'header_phone_number' );
}
// search 
function startkit_header_section_search_render_callback() {
	return get_theme_mod( 'header_search' );
}
// cart 
function startkit_header_section_cart_render_callback() {
	return get_theme_mod( 'header_cart' );
}
// book now button 
function startkit_header_section_booknow_render_callback() {
	return get_theme_mod( 'header_btn_lbl' );
}
?>