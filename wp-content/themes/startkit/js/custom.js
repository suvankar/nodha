(function($) {
    "use strict";
	jQuery(document).ready(function() {
        // Testimonial Carousel
        jQuery(".testimonial-carousel").owlCarousel({
            items: 3,
            loop: true,
            dots: true,
            nav: false,
            margin: 30,
            center: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                700: {
                    items: 1,
                },
                900: {
                    items: 3
                }
            }
        });	
        /* --------------------------------------
            Scroll UP
        -------------------------------------- */

        jQuery(window).on('scroll', function() {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });

        jQuery('.scrollup').on('click', function() {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

        // Search
        var changeClass = function(name) {
            $('#search').removeAttr('class').addClass(name);
        }

        /*------------------------------------
            Sticky Menu 
        --------------------------------------*/
        var windows = $(window);
        var stick = $(".header-sticky");
        windows.on('scroll', function() {
            var scroll = windows.scrollTop();
            if (scroll < 10) {
                stick.removeClass("sticky");
            } else {
                stick.addClass("sticky");
            }
        });
        /*------------------------------------
            jQuery MeanMenu 
        --------------------------------------*/
		
		jQuery('.mobile-menu-active').meanmenu({
            meanScreenWidth: "991",
            meanMenuContainer: '.mobile-menu'
        });

        /* last  2 li child add class */
        jQuery('ul.menu > li').slice(-2).addClass('last-elements');
	});



    jQuery(window).on('load', function() {

        // Sticky Nav
        jQuery(".sticky-nav").sticky({ topSpacing: 0 });

        // Preloader
        jQuery(".preloader").fadeOut('slow');

    });


}(jQuery));