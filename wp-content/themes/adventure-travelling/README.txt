=== Adventure Travelling ===
Contributors: ThemesPride
Tags: one-column, two-columns, right-sidebar, left-sidebar, three-columns, four-columns, grid-layout, custom-colors, custom-header, custom-background, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, full-width-template, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, blog, news, portfolio
Requires at least: 4.7
Tested up to: 4.9.8
Stable tag: 0.6.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adventure travelling is a bold, powerful, attractive and multipurpose travel WordPress theme for travel agency, tour planners, tour and tourist guides, travel and adventure blogs, travel magazines, tourist destination, tourist hotels and airline agencies and all the people related to tourism industry in one way or the other.

== Description ==

Adventure travelling is a bold, powerful, attractive and multipurpose travel WordPress theme for travel agency, tour planners, tour and tourist guides, travel and adventure blogs, travel magazines, tourist destination, tourist hotels and airline agencies and all the people related to tourism industry in one way or the other. The theme has an inviting and appealing design with banners and sliders adding to its beauty. Built on Bootstrap framework, it has a strong foundation with easy usage solution. Its user-friendly interface and well-structured layout lead to smooth navigation. It has a fluid layout which is responsive to varying screen sizes. To help serve in local languages, it is made translation ready. It is cross-browser compatible, SEO-friendly and lightweight for speedy loading. Made from scratch, it is bug-free to conform to the WordPress standards. Customization is a powerful tool provided by this theme to ensure good control over the site. Social media icons are linked with the theme to easily reach people.

Installation

1. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
2. Type in Adventure Travelling in the search form and press the 'Enter' key on your keyboard.
3. Click on the 'Activate' button to use your new theme right away.
4. Go to http://themespride.com/docs/adventure-traveling-lite for a guide on how to customize this theme.

== Changelog ==

= 0.1 =
	* Initial version released.

= 0.2 =
	* Resolved Theme Errors.
	* Added Theme url.

= 0.3 =
	* Done the changes in readme.txt file.
	* Done some code corrections.

= 0.4 =
	* Update the readme.txt file as per new updation.
	* Update screenshot.
	* Updated Woocommerce upto 3.5.0

= 0.5 =
	* Added language folder.
	* Changed the theme version in other files.
	* Changed the background color of menu in mobile media.

= 0.6 =
	* Changed the WordPress globals in the slider.php file.

= 0.6.1 =
	* Added the theme installation process in readme.txt file.
	* Resolved load_theme_textdomain in function.php 

= 0.6.2 =
	* Changed the language folder name to languages and also changed the name in function.php file.

= 0.6.3 =
	* Done The Readme.txt file changes.

= 0.6.4 =
	* Added the bootstarp.css.map file css folder.
	* Resolved the bootstrap console error of fire fox.

= 0.6.5 =
	* bootstrap.min.map removed as its not in use.

== Resources ==

Adventure Travelling WordPress Theme, Copyright 2018 ThemesPride
Adventure Travelling is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Adventure Travelling WordPress Theme is derived from Twenty Seventeen WordPress Theme, Copyright 2016 WordPress.org
Twenty Seventeen is distributed under the terms of the GNU GPL

Adventure Travelling bundles the following third-party resources:

* CSS bootstrap.css, License: Copyright 2011-2018 The Bootstrap Authors, Source: https://github.com/twbs/bootstrap/blob/master/LICENSE

* JS bootstrap.js, License: Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors), Source: https://github.com/twbs/bootstrap/blob/master/LICENSE

* Font Awesome icons, Copyright Dave Gandy, License: SIL Open Font License, version 1.1., Source: http://fontawesome.io/

* JS Superfish, License: https://github.com/joeldbirch/superfish

* Pixabay Images, License: CC0 1.0 Universal (CC0 1.0), Source: https://pixabay.com/en/service/terms/

* Banner image: Copyright dimitrisvetsikas1969, License: CC0 1.0 Universal (CC0 1.0), Source: https://pixabay.com/en/parachuting-water-sport-activity-2199840/

* Post image: Copyright miaalthoff, License: CC0 1.0 Universal (CC0 1.0), Source: https://pixabay.com/en/snow-winter-sport-skier-mountain-3090067/

* Post image: Copyright Pexels, License: CC0 1.0 Universal (CC0 1.0), Source: https://pixabay.com/en/adventure-altitude-backpack-climb-1850912/

* Post image: Copyright Pexels, License: CC0 1.0 Universal (CC0 1.0), Source: https://pixabay.com/en/adventure-landscape-mountain-1851076/